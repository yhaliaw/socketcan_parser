SocketCAN Parser
===
A real-time parser of SocketCAN frames/packets.
The project contains two file: `parser.py` and `sender.py`.
The file `parser.py` reads from the interface called `vcan0` and output infomation on the raw packets in real-time.
The file `sender.py` spams the `vcan0` interface with dummy frames/packets.

## Instructions
Test on Ubuntu with python 3.6 only.

- Create a virtual SocketCAN interface call `vcan0`.

  `sudo modprobe vcan`

  `sudo ip link add dev vcan0 type vcan`

  `sudo ip link set vcan0 up`

- Install python-can package. The tested version is 3.0.0.

  `pip install python-can=="3.0.0"`

- Run `parser.py` and then `sender.py` in different consoles.

  `python parser.py`

  `python sender.py`

  In the console window running `parser.py` should start receiving dummy frames/packets in real-time and parse them in real-time. Press `Ctrl + C` to exit either program.

Display statistic on `vcan0` interface with:

`ip -details -statistics link show vcan0`

Remove the `vcan0` interface with:

`sudo ip link del vcan0`