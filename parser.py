import struct
import can


bustype = 'socketcan'
channel = 'vcan0'

CANFD_MTU = 72
CAN_ERR_FLAG = 0x20000000
CAN_RTR_FLAG = 0x40000000
CAN_EFF_FLAG = 0x80000000
CANFD_BRS = 0x01
CANFD_ESI = 0x02
CAN_EXTENDED_ID = 0x1FFFFFFF
CAN_STANDARD_ID = 0x000007FF

HEADER_SIZE = 8


class SocketcanFrame:
    """
    Object to store attributes and data relating to SocketCAN frame.
    """
    def __init__(self, arbitration_id, is_extended, is_remote, is_error,
                 bitrate_switch, error_state_indicator, dlc, data):
        self.arbitration_id = arbitration_id
        self.is_extended = is_extended
        self.is_remote = is_remote
        self.is_error = is_error
        self.bitrate_switch = bitrate_switch
        self.error_state_indicator = error_state_indicator
        self.dlc = dlc
        self.data = data

    def __len__(self):
        """Returns the length of the frame."""
        return HEADER_SIZE + self.dlc


def parse(frame):
    """
    Parse a given SocketCAN frame with FD (flexible data rate) support.
    For SocketCAN, a frame is consist of:
        can_id: 32 bits CAN_ID + EFF/RTR/ERR flags
        can_dlc: 8 bits
        flags: 8 bits
        padding: 16 bits
        data: remaining (up to 8 bytes)
    See: https://www.kernel.org/doc/Documentation/networking/can.txt.

    :param frame: The frame to parse.
    :return: A SocketcanFrame object containing the attributes and data.
    """
    can_id, dlc, flags = struct.Struct("=IBB2x").unpack_from(frame)
    data = frame[HEADER_SIZE:HEADER_SIZE + dlc]

    # Bitwise add with mask to extract the bits containing information.
    is_error = bool(can_id & CAN_ERR_FLAG)
    is_remote = bool(can_id & CAN_RTR_FLAG)
    is_extended = bool(can_id & CAN_EFF_FLAG)

    bitrate_switch = bool(flags & CANFD_BRS)
    error_state_indicator = bool(flags & CANFD_ESI)

    if is_extended:
        arbitration_id = can_id & CAN_EXTENDED_ID
    else:
        arbitration_id = can_id & CAN_STANDARD_ID

    # Build a SocketcanFrame object with the attributes and data.
    parsed_frame = SocketcanFrame(arbitration_id=arbitration_id,
                                  is_extended=is_extended,
                                  is_remote=is_remote,
                                  is_error=is_error,
                                  bitrate_switch=bitrate_switch,
                                  error_state_indicator=error_state_indicator,
                                  dlc=dlc,
                                  data=data)
    return parsed_frame


def to_hex_string(bitstring):
    """Return a formatted hex string for given bitstring"""
    return ' '.join(format(x, '0>2X') for x in bytearray(bitstring))


if __name__ == "__main__":
    bus = can.interface.Bus(channel=channel, bustype=bustype)

    frame_count = 0

    print('Parser for SocketCAN frames/packets')
    print('Receiving frames/packets...\n')
    while True:
        frame = bus.socket.recv(CANFD_MTU)
        frame_count += 1

        print(f'Packet count: {frame_count}')

        parsed = parse(frame)

        frame_length = len(parsed)
        frame = frame[0:frame_length]  # Cut off the padding.

        hex_string = to_hex_string(frame)
        print(f'Packet Length: {frame_length} bytes.')
        print('Raw Packet in hex:')
        print(hex_string)

        header_hex_string = to_hex_string(frame[0:HEADER_SIZE])
        print('Header:')
        print(header_hex_string)
        print(f'Arbitration identifier: {parsed.arbitration_id}. ' +
              f'Extended identifier: {parsed.is_extended}.\n' +
              f'Remote frame: {parsed.is_remote}. ' +
              f'Error frame: {parsed.is_error}.\n' +
              f'Bitrate switch: {parsed.bitrate_switch}. ' +
              f'Error state indicator: {parsed.error_state_indicator}.\n' +
              f'Data length code: {parsed.dlc}.')

        payload_hex_string = to_hex_string(frame[HEADER_SIZE:])
        print('Payload:')
        print(payload_hex_string)
        print(f'Data: {parsed.data}.\n')
