import time
import can


bustype = 'socketcan'
channel = 'vcan0'

arbitration_id = [1, 2, 3947, 34, 498]
extended_id = [True, False, True, False, True]
data = [bytearray([1, 2, 3, 4, 5, 6, 7, 8]),
        bytearray([1, 2, 3, 4]),
        bytearray([0, 4, 3, 2, 3]),
        bytearray([7]),
        bytearray([0, 0, 0, 0, 1])]
is_error_frame = [False, False, False, True, False]
is_remote_frame = [False, False, False, False, True]


if __name__ == '__main__':
    bus = can.interface.Bus(channel=channel, bustype=bustype)

    while True:
        for i in range(5):
            msg = can.Message(arbitration_id=arbitration_id[i],
                              extended_id=extended_id[i],
                              data=data[i], dlc=len(data[i]),
                              is_error_frame=is_error_frame[i],
                              is_remote_frame=is_remote_frame[i])
            bus.send(msg)

            time.sleep(0.3)
        time.sleep(3)
